/**
 *
 */
/**
 * Interface for content row modifier function.
 */
interface ContentRowModifierFunction {
    (rowContentArray: Array<string>, row: HTMLTableRowElement): void;
}
/**
 * Header cell defintion.
 */
declare class HeaderCell {
    textValue: string;
    dataTablesortType: DataTablesortType;
    constructor(textValue: string, dataTablesortType: DataTablesortType);
}
/**
 * Table class definition.
 */
declare class Table {
    id: string;
    tableElement: HTMLTableElement;
    tableHeaderRowElement: HTMLTableRowElement;
    tBodyElement: HTMLTableSectionElement;
    /**
     * Creates new table element.
     * @param id unique identifier for table element.
     */
    constructor(id: string);
    /**
     * Adds text column to header
     * @param titleTxt header text
     * @param dataTablesortType option data type for sorting
     */
    addHeaderContent(headerCellArray: Array<HeaderCell>): void;
    /**
     * Adds text column to header
     * @param titleTxt header text
     * @param dataTablesortType option data type for sorting
     */
    private addHeaderTxtColumn;
    /**
     * Creates and returns new HTMLTableHeaderCellElement.
     * @param dataTablesortType option data type for sorting
     * @returns HTMLTableHeaderCellElement
     */
    private createHeaderColumn;
    /**
     * Ensures that thead and tr tag was created.
     */
    private ensureHeaderRowIsCreated;
    addContent(contentArray: Array<Array<string>>, contentRowModifierFunction?: ContentRowModifierFunction): void;
    private addContentRow;
    /**
     * Appends table to parent element.
     * @param parent Parent.
     */
    appendTo(parent: HTMLElement): void;
}
declare enum DataTablesortType {
    INT = "int",
    STRING = "string",
    DATE = "date",
    IGNORE = "ignore"
}
declare class TableFunctions {
    static init(): void;
    private static addEventListeners;
    private static tableSort;
    private static determineType;
    private static isNumeric;
    private static getCellIndex;
    private static sortRowsString;
    private static sortRowsStringDesc;
    private static sortRowsInt;
    private static sortRowsIntDesc;
    private static sortRowsDate;
    private static sortRowsDateDesc;
    private static isValidDate;
    private static loadAllTableText;
    private static loadTableText;
    private static tableSearch;
}
