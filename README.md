<h3 align="center">Sortable & searchable html tables:</h3>
<p align="center">Pure es2015 implementation für dynamic html tables, which are searchable and sortable. Based on jquery auto_table plugin (https://github.com/rrickgauer/auto-tables).</p>

<p align="center">This library based on the great Auto Tables Jquery Plugin. I just eliminated Jquery and customized it for my purpose:</p>
<p align="center"><strong><i><a href="https://github.com/rrickgauer/auto-tables">https://github.com/rrickgauer/auto-tables</a></i></strong></p>

