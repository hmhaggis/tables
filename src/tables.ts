/**
 * 
 */

/**
 * Interface for content row modifier function.
 */
interface ContentRowModifierFunction{
(rowContentArray: Array<string>, row: HTMLTableRowElement): void;
}

/**
 * Header cell defintion.
 */
class HeaderCell{
    textValue:string;
    dataTablesortType:DataTablesortType;
    public constructor(textValue:string,dataTablesortType:DataTablesortType){
        this.textValue = textValue;
        this.dataTablesortType = dataTablesortType;
    }
}

/**
 * Table class definition.
 */
 class Table  { 
    id:string; 
    tableElement:HTMLTableElement;
    tableHeaderRowElement:HTMLTableRowElement;
    tBodyElement:HTMLTableSectionElement;
  
    /**
     * Creates new table element.
     * @param id unique identifier for table element.
     */
     public constructor(id:string) { 
       this.id = id 
       this.tableElement = document.createElement("table");
       this.tableElement.setAttribute("id", id);
       this.tableElement.classList.add("tablesearch-table");
       this.tableElement.classList.add("tablesort");
    }  

     /**
      * Adds text column to header
      * @param titleTxt header text
      * @param dataTablesortType option data type for sorting
      */
     public addHeaderContent(headerCellArray:Array<HeaderCell>):void{
        for (var i = 0; i < headerCellArray.length; i++) {
            this.addHeaderTxtColumn(headerCellArray[i].textValue, headerCellArray[i].dataTablesortType);
        }
    }

     /**
      * Adds text column to header
      * @param titleTxt header text
      * @param dataTablesortType option data type for sorting
      */
     private addHeaderTxtColumn(titleTxt:string, dataTablesortType?:DataTablesortType):void{
         var newCol = this.createHeaderColumn(dataTablesortType);
         newCol.innerHTML=titleTxt;
     }

     /**
      * Creates and returns new HTMLTableHeaderCellElement.
      * @param dataTablesortType option data type for sorting
      * @returns HTMLTableHeaderCellElement
      */
     private createHeaderColumn(dataTablesortType?:DataTablesortType):HTMLTableHeaderCellElement{
        this.ensureHeaderRowIsCreated();
        var newCol = document.createElement("th");
        if(dataTablesortType){
            newCol.setAttribute("data-tablesort-type", dataTablesortType);
        }
        this.tableHeaderRowElement.appendChild(newCol);
        return newCol;
    }

    /**
     * Ensures that thead and tr tag was created.
     */
    private ensureHeaderRowIsCreated():void{
        if(!this.tableHeaderRowElement){
           var tHeadElement = document.createElement("thead");
           this.tableHeaderRowElement = document.createElement("tr");
           tHeadElement.appendChild(this.tableHeaderRowElement);
           this.tableElement.appendChild(tHeadElement);
        }
    }

    public addContent(contentArray:Array<Array<string>>, contentRowModifierFunction?: ContentRowModifierFunction):void{
        if(!this.tBodyElement){
            this.tBodyElement = document.createElement("tbody");
            this.tableElement.appendChild(this.tBodyElement);
        }
        for (var i = 0; i < contentArray.length; i++) {
            this.addContentRow(contentArray[i], contentRowModifierFunction);
        }
    }

    private addContentRow(contentRowArray:Array<string>, contentRowModifierFunction?: ContentRowModifierFunction):void{
        var newContentRow = document.createElement("tr");
        for (var i = 0; i < contentRowArray.length; i++) {
            var column = document.createElement("td");
            column.innerHTML = contentRowArray[i];
            newContentRow.appendChild(column);
        }

        if(contentRowModifierFunction){
            contentRowModifierFunction(contentRowArray, newContentRow);
        }
        this.tBodyElement.appendChild(newContentRow);
    }

    /**
     * Appends table to parent element.
     * @param parent Parent.
     */
     public appendTo(parent:HTMLElement):void{
        parent.appendChild(this.tableElement);
    }
 }

 
enum DataTablesortType {
    INT = "int",
    STRING = "string",
    DATE = "date",
    IGNORE = "ignore",
  }

class TableFunctions{

    public static init():void { 
        TableFunctions.loadAllTableText();
        TableFunctions.addEventListeners();
        var defaultSortColumns = document.querySelectorAll('.tablesort th.tablesort-default');
        for (var i = 0; i < defaultSortColumns.length; i++) {
            defaultSortColumns[i].dispatchEvent(new Event('click'));
        }
    }

    private static addEventListeners():void {
        var thElements = document.querySelectorAll('.tablesort th');
        for (var i = 0; i < thElements.length; i++) {
            let thElement = thElements[i];
            thElements[i].addEventListener('click', function() {
                TableFunctions.tableSort(thElement);
            });
        }

        var searchInputElement = document.querySelector('.tablesearch-input');
        searchInputElement.addEventListener('keyup', function() {
            TableFunctions.tableSearch(searchInputElement);
        });
    }

    private static tableSort(thClicked:Element):void {
        var table       = thClicked.closest('.tablesort');
        var columnIndex = TableFunctions.getCellIndex(thClicked as HTMLTableHeaderCellElement);
        var rows        = table.querySelectorAll('tbody tr');
        var sortType;

        // if no data type is specified, determine it
        if (thClicked.hasAttribute('data-tablesort-type'))
            sortType = thClicked.getAttribute('data-tablesort-type');
        else
            sortType = TableFunctions.determineType(thClicked as HTMLTableHeaderCellElement);

        if(sortType == DataTablesortType.IGNORE){
            return;
        }
        var thElements = table.querySelectorAll('thead th');
        if (thClicked.classList.contains('tablesort-asc')) {
            for (var i = 0; i < thElements.length; i++) {
                thElements[i].classList.remove('tablesort-asc');
                thElements[i].classList.remove('tablesort-desc');
            }
            thClicked.classList.add('tablesort-desc');

            switch (sortType) {
                case DataTablesortType.INT:
                    rows = TableFunctions.sortRowsIntDesc(rows, columnIndex);
                    break;
                case DataTablesortType.DATE:
                    rows = TableFunctions.sortRowsDateDesc(rows, columnIndex);
                    break;
                default:
                    rows = TableFunctions.sortRowsStringDesc(rows, columnIndex);
                    break;
            }
        } else {
            for (var i = 0; i < thElements.length; i++) {
                thElements[i].classList.remove('tablesort-asc');
                thElements[i].classList.remove('tablesort-desc');
            }
            thClicked.classList.add('tablesort-asc');

            switch (sortType) {
                case DataTablesortType.INT:
                    rows = TableFunctions.sortRowsInt(rows, columnIndex);
                    break;
                case DataTablesortType.DATE:
                    rows = TableFunctions.sortRowsDate(rows, columnIndex);
                    break;
                default:
                    rows = TableFunctions.sortRowsString(rows, columnIndex);
                    break;
            }
        }
        var fragment = new DocumentFragment();
        for (var i = 0; i < rows.length; i++) {
            fragment.appendChild(rows[i]);
        }

        var tBodyElement = table.querySelector('tbody');
        tBodyElement.innerHTML = "";
        tBodyElement.appendChild(fragment);
    }


    private static determineType(thClicked:HTMLTableHeaderCellElement):DataTablesortType {
        var table       = thClicked.closest('.tablesort');
        var columnIndex = TableFunctions.getCellIndex(thClicked);
        var rows        = table.querySelectorAll('tbody tr');
        var isString    = false;

        for (var count = 0; count < rows.length; count++) {
            var cells = rows[count].querySelectorAll('td');
            var cellValue = cells[columnIndex].textContent;

            if (TableFunctions.isNumeric(cellValue))
                isString = true;
        }

        if (isString)
            return DataTablesortType.STRING;
        else
            return DataTablesortType.INT;
    }

    private static isNumeric(n:string):boolean {
        return !isNaN(parseFloat(n)) && isFinite(parseFloat(n));
    }

    private static getCellIndex(th:HTMLTableHeaderCellElement):number {
        return th.cellIndex;
    }
 
    private static sortRowsString(rows, columnIndex):any {
        var sortedRows = Array.prototype.slice.call(rows).sort(function(a, b) {
            var cellsA = a.querySelectorAll('td');
            var cellsB = b.querySelectorAll('td');
            var textA  = cellsA[columnIndex].textContent.toUpperCase();
            var textB  = cellsB[columnIndex].textContent.toUpperCase();

            return textA < textB ? -1 : 1;
        });

        return sortedRows;
    }

    private static sortRowsStringDesc(rows, columnIndex):any {
        var sortedRows = Array.prototype.slice.call(rows).sort(function(a, b) {
            var cellsA = a.querySelectorAll('td');
            var cellsB = b.querySelectorAll('td');
            var textA  = cellsA[columnIndex].textContent.toUpperCase();
            var textB  = cellsB[columnIndex].textContent.toUpperCase();

            return textA > textB ? -1 : 1;
        });

        return sortedRows;
    }

    private static sortRowsInt(rows, columnIndex):any {
        var sortedRows = Array.prototype.slice.call(rows).sort(function(a, b) {
            var cellsA = a.querySelectorAll('td');
            var cellsB = b.querySelectorAll('td');
            var numA  = parseFloat(cellsA[columnIndex].textContent.trim().replace(' ', '').replace(',', '.'));
            var numB  = parseFloat(cellsB[columnIndex].textContent.trim().replace(' ', '').replace(',', '.'));

            return numA < numB ? -1 : 1;
        });

        return sortedRows;
    }

 
    private static sortRowsIntDesc(rows: NodeListOf<Element>, columnIndex: number):any {
        var sortedRows = Array.prototype.slice.call(rows).sort(function(a, b) {
            var cellsA = a.querySelectorAll('td');
            var cellsB = b.querySelectorAll('td');
            var numA  = parseFloat(cellsA[columnIndex].textContent.trim().replace(' ', '').replace(',', '.'));
            var numB  = parseFloat(cellsB[columnIndex].textContent.trim().replace(' ', '').replace(',', '.'));

            return numA > numB ? -1 : 1;
        });

        return sortedRows;
    }

    private static sortRowsDate(rows, columnIndex):any  {
        var sortedRows = Array.prototype.slice.call(rows).sort(function(a, b) {
            var cellsA = a.querySelectorAll('td');
            var cellsB = b.querySelectorAll('td');
            var dateA = new Date(cellsA[columnIndex].textContent);
            var dateB = new Date(cellsB[columnIndex].textContent);

            return dateA < dateB ? -1 : 1;
        });

        return sortedRows;
    }

    private static sortRowsDateDesc(rows, columnIndex):any {
        var sortedRows = Array.prototype.slice.call(rows).sort(function(a, b) {
            var cellsA = a.querySelectorAll('td');
            var cellsB = b.querySelectorAll('td');
            var dateA = new Date(cellsA[columnIndex].c);
            var dateB = new Date(cellsB[columnIndex].textContent);

            return dateA > dateB ? -1 : 1;
        });

        return sortedRows;
    }

    private static isValidDate(dateString):boolean {
        return isFinite(parseFloat(dateString));
    }

    private static loadAllTableText():void {
        var tablesearchTables = document.querySelectorAll('.tablesearch-table');

        for (var count = 0; count < tablesearchTables.length; count++)
        TableFunctions.loadTableText(tablesearchTables[count]);
    }

    private static loadTableText(table:Element):void {
        var cells = table.querySelectorAll('tbody td.tablesearch-source');
        if(cells.length === 0) {
            // fallback to all cells
            cells = table.querySelectorAll('tbody td');
        }

        for (var count = 0; count < cells.length; count++) {
            var cell = cells[count];
            var upperCaseText = cell.textContent.trim().toUpperCase();
            cell.setAttribute('data-tablesearch-text', upperCaseText);
        }
    }

    private static tableSearch(input):void {
        var text = input.value.toUpperCase();
        var tableId = input.getAttribute('data-tablesearch-table');
        var table = document.getElementById(tableId);
        var trElements = table.querySelectorAll('tbody tr');
        if (!text || text == '' || text.length == 0) {
            for (var count = 0; count < trElements.length; count++) {
                trElements[count].classList.remove('hidden');
            } 
            return;
        }

        for (var count = 0; count < trElements.length; count++) {
            trElements[count].classList.add('hidden');
        }
        var tdElements = table.querySelectorAll('tbody td[data-tablesearch-text*="' + text + '"]');
        for (var count = 0; count < tdElements.length; count++) {
            tdElements[count].closest('tr').classList.remove('hidden');
        }
    }
}